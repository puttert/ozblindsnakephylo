# sqcl_tree.R


library(ape)
library(phytools)


# load data ---------------------------------------------------------------

ahe_tree <- read.tree(file = 'data/tree/20220309_all_Loci_AHE_ASTRAL.tre')
uce_tree <- read.tree(file = 'data/tree/20220310_uce_ASTRAL.tre')
all_tree <- read.tree(file = 'data/tree/20220309_all_loci_tree.tre')



par(mar=c(0.1,0.1,0.1,0.1))
plot(ahe_tree)
dev.off()

plotTree(ahe_tree)

ahe_tree$edge.length

# analysis_diversification_model_fit.R
# Putter Tiatragul
# Started on 27 May 2022

# We have:
# -- We have phylogenomic time-calibrated tree (SqCL probe)
# -- Paleoclimate reconstruction by Alex Skeels

# libraries ---------------------------------------------------------------

library(tidyverse)
library(RPANDA)
library(phytools); library(ape)
library(gridExtra); library(ggplot2)

# phylogeny ---------------------------------------------------------------
# Time calibrated tree from SqCL probe and mcmctree analysis with fossils

# Full tree with fossils
fos_tree_full <- ape::read.nexus("data/tree/20220420mcmctree.tre")

# tips we want for blindsnakes
blindsnakes_samples <- c('Anilios_affinis_J93635','Anilios_ammodytes_R158097','Anilios_aspina_J91822','Anilios_australis_R115859',
                         'Anilios_bicolor_R55342','Anilios_bituberculatus_R63990','Anilios_broomi_J87805','Anilios_centralis_R14631',
                         'Anilios_diversus_R112027','Anilios_endoterus_R102725','Anilios_ganei_R165000', 
                         'Anilios_grypus_R108596','Anilios_grypus_R157297','Anilios_grypus_R55272',
                         'Anilios_guentheri_R108431','Anilios_hamatus_R136277','Anilios_howi_R146381','Anilios_kimberleyensis_R164213', 
                         'Anilios_leptosoma_R119241','Anilios_leucoproctus_J87547', 
                         'Anilios_ligatus_R31019','Anilios_longissimus_R120049','Anilios_margaretae_R163269',
                         'Anilios_nigrescens_R31022','Anilios_obtusifrons_R146400','Anilios_pilbarensis_R108813',
                         'Anilios_pinguis_R146995','Anilios_polygrammicus_R98715','Anilios_proximus_R132486', 
                         'Anilios_silvia_J46128','Anilios_splendidus_R119900','Anilios_systenos_R114894', 
                         'Anilios_torresianus_J47640','Anilios_tovelli_R21063','Anilios_troglodytes_R146048', 
                         'Anilios_unguirostris_R21669','Anilios_waitii_R113302','Anilios_wiedii_J59020', 
                         'Anilios_yirrikalae_J85695','Ramphotyphlops_cfwaitii_R51244', 
                         'Anilios_ligatus_R19109'
                         )

# Subset the tree
fos_tree <- ape::keep.tip(phy = fos_tree_full, tip = blindsnakes_samples)

# Rename tips (for grypus we have three potential species)
fos_tree$tip.label[which(fos_tree$tip.label %in% c('Anilios_grypus_R108596','Anilios_grypus_R157297','Anilios_grypus_R55272'))] <- c('Anilios_grypusW_R108596', 'Anilios_grypusNW_R157297')

# Just keep the species name (without rego)
fos_tree$tip.label <- stringr::str_replace(string = fos_tree$tip.label, pattern = regex("_([A-z][0-9]+)"), replacement = "")

# Climate data ------------------------------------------------------------



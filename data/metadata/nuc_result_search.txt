gene_name, assession, GI_number
Anilios ammodytes voucher WAM R102560 cytochrome b gene, partial cds; mitochondrial, KC489800.1, GI:451763240
Anilios ammodytes voucher WAM R110199 cytochrome b gene, partial cds; mitochondrial, KC489806.1, GI:451763252
Anilios ammodytes voucher WAM R124837 cytochrome b gene, partial cds; mitochondrial, KC489821.1, GI:451763282
Anilios ammodytes voucher WAM R110199 prolactin receptor gene, partial cds, KC490435.1, GI:451764510
Anilios ammodytes voucher WAM R124837 prolactin receptor gene, partial cds, KC490443.1, GI:451764526
Anilios ammodytes voucher WAM:R102560 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992954.1, GI:576911144
Anilios ammodytes voucher WAM:R165282 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992955.1, GI:576911145
Anilios ammodytes voucher WAM:R139430 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992956.1, GI:576911146
Anilios ammodytes voucher WAM:R141306 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992958.1, GI:576911148
Anilios ammodytes voucher WAM:R158097 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992959.1, GI:576911149
Anilios ammodytes voucher WAM:R127760 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992960.1, GI:576911150
Anilios ammodytes voucher WAM:R102560 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993041.1, GI:576911231
Anilios ammodytes voucher WAM:R165282 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993042.1, GI:576911232
Anilios ammodytes voucher WAM:R139430 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993043.1, GI:576911233
Anilios ammodytes voucher WAM:R141306 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993045.1, GI:576911235
Anilios ammodytes voucher WAM:R158097 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993046.1, GI:576911236
Anilios ammodytes voucher WAM:R127760 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993047.1, GI:576911237
Anilios bicolor voucher WAM:R165618 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992965.1, GI:576911155
Anilios bicolor voucher WAM:R165618 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993052.1, GI:576911242
Anilios ganei voucher WAM R156328 cytochrome b gene, partial cds; mitochondrial, KC490218.1, GI:451764076
Anilios ganei voucher WAM:R140003 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992986.1, GI:576911176
Anilios ganei voucher WAM:R124835 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992988.1, GI:576911178
Anilios ganei voucher WAM:R124835 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993073.1, GI:576911263
Anilios ganei voucher WAM-R165000 cytochrome b (cytb) gene, partial cds; mitochondrial, KT316489.1, GI:948563497
Anilios longissimus voucher WAM:R120049 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993107.1, GI:576911297
Anilios pilbarensis voucher WAM R108813 cytochrome b gene, partial cds; mitochondrial, KC490367.1, GI:451764374
Anilios pilbarensis voucher WAM R110860 cytochrome b gene, partial cds; mitochondrial, KC490371.1, GI:451764382
Anilios pilbarensis voucher WAM R108813 prolactin receptor gene, partial cds, KC490859.1, GI:451765358
Anilios pilbarensis voucher WAM R110860 prolactin receptor gene, partial cds, KC490863.1, GI:451765366
Anilios splendidus voucher WAM-R119900 cytochrome b (cytb) gene, partial cds; mitochondrial, KT316497.1, GI:948563513
Anilios waitii voucher WAM R113302 cytochrome b gene, partial cds; mitochondrial, KC490405.1, GI:451764450
Anilios waitii voucher WAM R136741 cytochrome b gene, partial cds; mitochondrial, KC490419.1, GI:451764478
Anilios waitii voucher WAM R113302 prolactin receptor gene, partial cds, KC490891.1, GI:451765422
Anilios centralis voucher SAMA:R56511 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992971.1, GI:576911161
Anilios centralis voucher SAMA:R56511 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993058.1, GI:576911248
Anilios unguirostris voucher WAM R115861 cytochrome b gene, partial cds; mitochondrial, KC490398.1, GI:451764436
Anilios unguirostris voucher WAM R115861 prolactin receptor gene, partial cds, KC490887.1, GI:451765414
Anilios unguirostris voucher NTM:R21669 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993031.1, GI:576911221
Anilios unguirostris voucher SAMA:R54430 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993032.1, GI:576911222
Anilios unguirostris voucher NTM:R21669 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993121.1, GI:576911311
Anilios unguirostris voucher SAMA:R54430 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993122.1, GI:576911312
Anilios endoterus voucher WAM R166251 cytochrome b gene, partial cds; mitochondrial, KC490171.1, GI:451763982
Anilios endoterus voucher WAM R102725 cytochrome b gene, partial cds; mitochondrial, KC490206.1, GI:451764052
Anilios endoterus voucher WAM R114998 cytochrome b gene, partial cds; mitochondrial, KC490208.1, GI:451764056
Anilios endoterus voucher WAM R127182 cytochrome b gene, partial cds; mitochondrial, KC490210.1, GI:451764060
Anilios endoterus voucher WAM R166251 prolactin receptor gene, partial cds, KC490709.1, GI:451765058
Anilios endoterus voucher WAM R102725 prolactin receptor gene, partial cds, KC490735.1, GI:451765110
Anilios endoterus voucher WAM R114998 prolactin receptor gene, partial cds, KC490737.1, GI:451765114
Anilios endoterus voucher WAM R127182 prolactin receptor gene, partial cds, KC490739.1, GI:451765118
Anilios endoterus voucher WAM:R102627 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992983.1, GI:576911173
Anilios endoterus voucher WAM:R102627 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993070.1, GI:576911260
Mauremys japonica mitochondrial cytb gene for cytochrome b, partial cds, specimen_voucher: KUZ:R63990, AB559119.1, GI:310688289
Mauremys japonica mitochondrial DNA, D-loop, partial sequence, specimen_voucher: KUZ:R63990, AB559357.1, GI:310688662
Anilios howi voucher WAM-R146381 cytochrome b (cytb) gene, partial cds; mitochondrial, KT316493.1, GI:948563505
Anilios kimberleyensis voucher WAM R133192 cytochrome b gene, partial cds; mitochondrial, KC490334.1, GI:451764308
Anilios kimberleyensis voucher WAM R164213 cytochrome b gene, partial cds; mitochondrial, KC490337.1, GI:451764314
Anilios kimberleyensis voucher WAM R133192 prolactin receptor gene, partial cds, KC490831.1, GI:451765302
Anilios kimberleyensis voucher WAM R164213 prolactin receptor gene, partial cds, KC490834.1, GI:451765308
Anilios kimberleyensis voucher WAM:R125981 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993013.1, GI:576911203
Anilios kimberleyensis voucher WAM:R125981 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993100.1, GI:576911290
Sundatyphlops polygrammicus voucher WAM:R98715 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993038.1, GI:576911228
Sundatyphlops polygrammicus voucher WAM:R98715 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993128.1, GI:576911318
Sundatyphlops polygrammicus voucher WAM-R98715 cytochrome b (cytb) gene, partial cds; mitochondrial, KT316551.1, GI:948563621
Anilios grypus voucher WAM R102196 cytochrome b gene, partial cds; mitochondrial, KC490226.1, GI:451764092
Anilios grypus voucher WAM R110246 cytochrome b gene, partial cds; mitochondrial, KC490234.1, GI:451764108
Anilios grypus voucher WAM R157297 cytochrome b gene, partial cds; mitochondrial, KC490271.1, GI:451764182
Anilios grypus voucher WAM:R114909 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992989.1, GI:576911179
1Anilios grypus voucher WAM:R108596 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992990.1, GI:576911180
1Anilios grypus voucher WAM:R157403 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992991.1, GI:576911181
1Anilios grypus voucher WAM:R108923 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992992.1, GI:576911182
1Anilios grypus voucher WAM:R102679 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992996.1, GI:576911186
1Anilios grypus voucher SAMA:R55272 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992997.1, GI:576911187
1Anilios grypus voucher WAM:R114909 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993074.1, GI:576911264
1Anilios grypus voucher WAM:R108596 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993075.1, GI:576911265
1Anilios grypus voucher WAM:R157403 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993076.1, GI:576911266
1Anilios grypus voucher WAM:R108923 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993077.1, GI:576911267
1Anilios grypus voucher WAM:R102679 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993081.1, GI:576911271
1Anilios grypus voucher SAMA:R55272 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993082.1, GI:576911272
1Anilios leptosomus voucher WAM R114892 cytochrome b gene, partial cds; mitochondrial, KC490339.1, GI:451764318
1Anilios leptosomus voucher WAM R114892.2 cytochrome b gene, partial cds; mitochondrial, KC490340.1, GI:451764320
1Anilios leptosomus voucher WAM R114893 cytochrome b gene, partial cds; mitochondrial, KC490341.1, GI:451764322
1Anilios leptosomus voucher WAM R146400 cytochrome b gene, partial cds; mitochondrial, KC490343.1, GI:451764326
1Anilios leptosomus voucher WAM R114892 prolactin receptor gene, partial cds, KC490836.1, GI:451765312
1Anilios leptosomus voucher WAM R114892.2 prolactin receptor gene, partial cds, KC490837.1, GI:451765314
1Anilios leptosomus voucher WAM R114893 prolactin receptor gene, partial cds, KC490838.1, GI:451765316
1Anilios leptosomus voucher WAM R146400 prolactin receptor gene, partial cds, KC490840.1, GI:451765320
1Anilios leptosomus voucher WAM:R114894 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993014.1, GI:576911204
1Anilios leptosomus voucher WAM:R129778 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993015.1, GI:576911205
1Anilios leptosomus voucher WAM:R114894 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993101.1, GI:576911291
1Anilios leptosomus voucher WAM:R129778 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993102.1, GI:576911292
1Anilios diversus voucher WAM R112993 cytochrome b gene, partial cds; mitochondrial, KC490123.1, GI:451763886
1Anilios diversus voucher WAM R112993 prolactin receptor gene, partial cds, KC490670.1, GI:451764980
1Anilios diversus voucher WAM:R157402 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992973.1, GI:576911163
1Anilios diversus voucher NTM:R16427 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992974.1, GI:576911164
1Anilios diversus voucher WAM:R151035 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992976.1, GI:576911166
1Anilios diversus voucher WAM:R112027 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992980.1, GI:576911170
1Anilios diversus voucher NTM:R19058 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992981.1, GI:576911171
1Anilios diversus voucher WAM:R157402 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993060.1, GI:576911250
1Anilios diversus voucher NTM:R16427 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993061.1, GI:576911251
1Anilios diversus voucher WAM:R151035 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993063.1, GI:576911253
1Anilios diversus voucher WAM:R112027 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993067.1, GI:576911257
1Anilios diversus voucher NTM:R19058 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993068.1, GI:576911258
1Anilios hamatus voucher WAM R110958 cytochrome b gene, partial cds; mitochondrial, KC490297.1, GI:451764234
1Anilios hamatus voucher WAM R112742 cytochrome b gene, partial cds; mitochondrial, KC490303.1, GI:451764246
1Anilios hamatus voucher WAM R127804 cytochrome b gene, partial cds; mitochondrial, KC490308.1, GI:451764256
1Anilios hamatus voucher WAM R110958 prolactin receptor gene, partial cds, KC490800.1, GI:451765240
1Anilios hamatus voucher WAM R112742 prolactin receptor gene, partial cds, KC490806.1, GI:451765252
1Anilios hamatus voucher WAM R127804 prolactin receptor gene, partial cds, KC490811.1, GI:451765262
1Anilios hamatus voucher WAM:R156249 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993003.1, GI:576911193
1Anilios hamatus voucher WAM:R111862 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993006.1, GI:576911196
1Anilios hamatus voucher WAM:R136276 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993007.1, GI:576911197
1Anilios hamatus voucher WAM:R156249 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993090.1, GI:576911280
1Anilios hamatus voucher WAM:R111862 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993093.1, GI:576911283
1Anilios hamatus voucher WAM:R136276 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993094.1, GI:576911284
1Anilios bituberculatus voucher SAMA:R44731 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992968.1, GI:576911158
1Anilios bituberculatus voucher SAMA:R44731 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993055.1, GI:576911245
1Anilios ligatus voucher NTM:R35156 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993017.1, GI:576911207
1Anilios ligatus voucher NTM:R19109 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993018.1, GI:576911208
1Anilios ligatus voucher SAMA:R31019 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993103.1, GI:576911293
1Anilios ligatus voucher NTM:R35156 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993105.1, GI:576911295
1Anilios ligatus voucher NTM:R19109 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993106.1, GI:576911296
1Anilios pinguis voucher WAM:R146995 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993114.1, GI:576911304
1Anilios troglodytes voucher WAM:R146048 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993029.1, GI:576911219
1Anilios troglodytes voucher WAM:R146048 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993119.1, GI:576911309
1Anilios affinis voucher SAMA:R55645 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992951.1, GI:576911141
1Anilios australis voucher WAM R115859 cytochrome b gene, partial cds; mitochondrial, KC489910.1, GI:451763460
1Anilios australis voucher WAM R132006 cytochrome b gene, partial cds; mitochondrial, KC489962.1, GI:451763564
1Anilios australis voucher WAM R115859 prolactin receptor gene, partial cds, KC490499.1, GI:451764638
1Anilios australis voucher WAM R132006 prolactin receptor gene, partial cds, KC490542.1, GI:451764724
1Anilios australis voucher WAM:R154969 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992963.1, GI:576911153
1Anilios australis voucher WAM:R154969 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993050.1, GI:576911240
1Anilios guentheri voucher NTM:R16488 12S ribosomal RNA gene, partial sequence; mitochondrial, KF992998.1, GI:576911188
1Anilios guentheri voucher SAMA:R53885 12S ribosomal RNA gene, partial sequence; mitochondrial, KF993001.1, GI:576911191
1Anilios guentheri voucher WAM:R105974 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993083.1, GI:576911273
1Anilios guentheri voucher WAM:R108431 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993084.1, GI:576911274
1Anilios guentheri voucher NTM:R16488 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993085.1, GI:576911275
1Anilios guentheri voucher SAMA:R53885 16S ribosomal RNA gene, partial sequence; mitochondrial, KF993088.1, GI:576911278

# Information about each tree file here.

## Fossil calibrated MCMCTREEs
### How was it made: MCMCTREE analysis using SqCL ASTRAL tree, partition files and fossil calibrations as inputs.
### Original source: `putter@nick.rsb.anu.edu.au/home/putter/SqCL_Pipeline/Anilios/mcmc_analysis`
### Usage: Diversification analyses with RPANDA

- 20220309_all_Loci_AHE_ASTRAL_nex.tre -- just AHE loci
- 20220309_all_Loci_AHE_ASTRAL.tre -- AHE loci
- 20220309_all_loci_tree_nex.tre -- all SqCL loci
- 20220309_all_loci_tree.tre -- all SqCL loci
- 20220310_uce_ASTRAL.tre -- just UCE loci
- 20220311AHE_concatenated.out -- output from 
- 20220420_mcmctree_fos_cal.tre -- raw fossil calibrated tree from all loci
- 20220420mcmctree.tre -- fossil calibrated tree from all loci

## cytb tree, not time calibrated
### How was it made: IQTREE run from alignments of cytb sequences from GenBank (provided by Ian Brennan) + some mtGenomes extracted cytb from our SqCl data 
### Original source: `putter@nick.rsb.anu.edu.au/home/putter/SqCL_Pipeline/mtGenome_blindsnake2`
### Usage: Map distribution of samples specimens
### Code: 03_mitotree_genotyped_locality.R

- Anilios_species_cytb_genbank_sqcl.tre 
- 20220524_Anilios_cytb_GMSU_genbank_sqcl.tre

## Time calibrated PRLR and cytb tree
### How was it made: IQTREE run from alignments of cytb + PRLR sequences from GenBank and time calibrated (help from Carlos Pavon Vazquez)
### Original source: typhlop.phylo
### Usage: preliminary analyses

- MCC_10_meanh.tre
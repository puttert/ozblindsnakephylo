### What is this repository for? ###
This repository contains the files for chapter 1 of ST's PhD Thesis at the Australian National University. 
The main objective is to infer a phylogeny for Australian blind snakes in the genus *Anilios* and estimate diversification rates of the continental Australian radiation. 

### How do I get set up? ###
All analysis performed in *R*. 

### Who do I talk to? ###
Contact Sarin Tiatragul (sarin.tiatragul at anu.edu.au)

### Collaborators
Alex Skeels - reconstructed paleo-climate data